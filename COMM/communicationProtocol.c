#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "../global.h"

static uint8_t getHeader(const uint8_t *inputBuffer);
static uint8_t getInfoField(const uint8_t *inputBuffer);
static unsigned short getData(const uint8_t *inputBuffer);
static uint8_t  getCrc(const uint8_t *inputBuffer);

static uint8_t getHeader(const uint8_t *inputBuffer) {
	return (*(inputBuffer));
}

static uint8_t getInfoField(const uint8_t *inputBuffer) {
	return (*(inputBuffer + 1));
}

static unsigned short getData(const uint8_t *inputBuffer) {
	unsigned short aux = (*(inputBuffer + 2));
	aux = aux << 8;
	aux |= (*(inputBuffer + 3));
	return aux;
}
static uint8_t  getCrc(const uint8_t *inputBuffer) {
	return (*(inputBuffer + 4));
}

/* se apeleaza cand cineva vrea sa trimita catre telefon*/
uint8_t *COM_encodeBuffer(const uint8_t interface, uint16_t outputData) {
	uint8_t *outputBuffer = (uint8_t*)malloc(MAX_LEN_PROTOCOL);
	outputBuffer[0] = SEND;
	outputBuffer[1] = interface;
	outputBuffer[2] = (uint8_t) ((outputData & 0xFF00) >> 8);
	outputBuffer[3] = (uint8_t) (outputData & 0x00FF);
	outputBuffer[4] = COM_STACK_CRC(0, outputBuffer, 4); //crc pentru primii 4 bytes
	return outputBuffer;
}

tProtocol *COM_decodeBuffer(const uint8_t *inputBuffer) {
	tProtocol *result = (tProtocol*)malloc(sizeof(tProtocol)); // trebuie sa fac chest

	result->head       = getHeader(inputBuffer);
	result->interface  = getInfoField(inputBuffer);
	result->data[0]    = (uint8_t)((0x00FF) & (getData(inputBuffer)));
	result->data[1]    = (uint8_t)(((0xFF00) & (getData(inputBuffer))) >> 8);
	result->crc        = getCrc(inputBuffer);
	printf("%u, %u", inputBuffer[0], inputBuffer[1]);
	printf("message decoded\n");
	fflush(stdout);
	return result;
}
