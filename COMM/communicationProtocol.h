#ifndef COM_PROTOCOL
#define COM_PROTOCOL
#define MASK  0b11111111 // 1515 0xFF
//head
#define RECEIVE     0b00000001 // 1 0x01
#define SEND       0b00000010 // 2 0x02

//interface
#define FORWARD 1
#define BACKWARD 2
#define MAKELEFT 3
#define MAKERIGHT 4
#define STOP 5
#define SPEED       6
#define SIGN        7
#define SENSOR1 8
#define SENSOR2     9
#define SENSOR3     10
#define SENSOR4     11
#define SENSOR5     12
#define SENSOR6     13

typedef unsigned char uint8_t;
typedef struct {
	uint8_t head;
	uint8_t interface;
	uint8_t data[2];
	uint8_t crc;
	const uint8_t reserved[3];
} tProtocol;

uint8_t *COM_encodeBuffer(const uint8_t interface, uint16_t outputData);
tProtocol *COM_decodeBuffer(const uint8_t *inputBuffer);

#endif
