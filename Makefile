CC=gcc
OFLAGS=-Wall -pthread -lncurses -lwiringPi -Ofast -o 
CFLAGS=-Wall -c

M_DRIVER_DIR=HDRV/M_D/src
BT_DRIVER_DIR=HDRV/BT_D/src
ENC_DRIVER_DIR=HDRV/ENC_D/src
SEN_DRIVER_DIR=HDRV/SEN_D/src
SIGN_DRIVER_DIR=HDRV/SIGN_D


COM_STACK_DIR=COMM
EVENT_MAN_DIR=MAN/EVEXEC/src
HANDLERS_DIR=.
MAIN_DIR=.


all: main.o bt_driver.o m_driver.o event.o communicationProtocol.o crc.o enc_driver.o sen_driver.o sign_socket.o
	$(CC) $(OFLAGS) executabil main.o  bt_driver.o event.o \
			 m_driver.o communicationProtocol.o crc.o \
			 enc_driver.o sen_driver.o sign_socket.o

main.o:
	$(CC) $(CFLAGS) $(MAIN_DIR)/main.c

bt_driver.o:
	$(CC) $(CFLAGS) $(BT_DRIVER_DIR)/bt_driver.c

m_driver.o:
	$(CC) $(CFLAGS) $(M_DRIVER_DIR)/m_driver.c

event.o:
	$(CC) $(CFLAGS) $(EVENT_MAN_DIR)/event.c

eventList.o:
	$(CC) $(CFLAGS) $(EVENT_MAN_DIR)/eventList.c


communicationProtocol.o:
	$(CC) $(CFLAGS) $(COM_STACK_DIR)/communicationProtocol.c

crc.o:
	$(CC) $(CFLAGS) $(COM_STACK_DIR)/crc.c

enc_driver.o:
	$(CC) $(CFLAGS) $(ENC_DRIVER_DIR)/enc_driver.c

sen_driver.o:
	$(CC) $(CFLAGS) $(SEN_DRIVER_DIR)/sen_driver.c

sign_socket.o:
	$(CC) $(CFLAGS) $(SIGN_DRIVER_DIR)/sign_socket.c

clean:
	rm *.o executabil
