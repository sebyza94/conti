#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../../global.h"



void _postEvent(Event *e) {
	addItemToList(e);
}
void execEvent(Event *event)
{
	printf("exectEvent");fflush(stdout);
        EventState eventState = event->handler();
        if(eventState == EVENT_HANDLED)
                removeTop(topList);

}

void* EXC_run(void *arg) /* Va rula intr-un thread separat*/
{
	printf("Execution event running...\n");fflush(stdout);
	while(1){
		if(topList != NULL){
		execEvent(topList->event);
		}
	}
}

pEventList makeEventNode(Event *event) {
	pEventList item = (pEventList) malloc (sizeof(EventList));
	/* Allocate memory for a new node */
	item->event     = event;
	item->nextEvent = NULL;
	return item;
}


void removeEventNode(Event* event) {

	pEventList currentNode = topList;
	/* Current node of the list is the top of the list */

	pEventList previousNode = NULL;
	/* For the first time the previous node is empty
	   because we point to the first element of the list*/

	while (currentNode != NULL)
	{
		/* While there are nodes, I mean, while the list is not empty */
		if (currentNode->event == event)
		{	/* If we found the element */

			if (previousNode == NULL)
				topList = currentNode->nextEvent;
			else
				previousNode->nextEvent = currentNode->nextEvent;

			free(currentNode);
		}
		previousNode = currentNode;
		currentNode  = currentNode->nextEvent;
	}
}

void removeTop() {
	pEventList currentNode = topList;
	topList = topList->nextEvent;
	free(currentNode);
}

uint32_t getEventListSize(pEventList topList) {

	pEventList currentList = topList;
	uint32_t size = 0;

	while (currentList != NULL)
	{
		size++;
		currentList = currentList->nextEvent;
	}

	return size;
}

void addItemToList(Event *event) {
	pEventList newItem = makeEventNode(event);

	if (topList == NULL)
	{	/* topList = NULL means the list is empty */
		topList = newItem;
	} else {
		/* put the new element in the tail */
		pEventList currentNode = topList;

		while (currentNode->event->type < event->type) {
			currentNode  = currentNode->nextEvent;
		}
		newItem->nextEvent = currentNode->nextEvent;
		currentNode->nextEvent = newItem;
	}
	printf("added Item to list\n");fflush(stdout);
}

void displayList() {
	pEventList currentNode = topList;
	while (currentNode != NULL) {
		printf("type:%d, state:%d \n", currentNode->event->type, currentNode->event->state); fflush(stdout);
		currentNode = currentNode->nextEvent;
	}
}
