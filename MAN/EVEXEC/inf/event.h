#ifndef EVENT
#define EVENT

typedef enum type {
	EVENT_COMM_ = 5,          /* Highest priority */
	EVENT_SEN_  = 30,          /* Sensors for collision */
	EVENT_SIGN_ = 2,          /* Sign detection */
	EVENT_BT_   = 5,
	EVENT_ENC_  = 2,
} EventType;

typedef enum state {
	EVENT_NOT_HANDLED = 0,
	EVENT_HANDLED  = 1
} EventState;

typedef struct {
	EventType type;
	EventState state;
	unsigned char (*handler)();
} Event;

typedef struct EventList
{
	Event* event;
	struct EventList* nextEvent;

} EventList;
typedef EventList *pEventList;

pEventList topList;

void _postEvent(Event *e);
void* EXC_run(void *arg);
pEventList makeEventNode(Event *event);
void removeEventNode( Event* event);
void removeTop();
uint32_t getEventListSize();
void addItemToList(Event *event);
void displayList();


#endif
