import numpy as np
import cv2

cap = cv2.VideoCapture(0)
stopSign_cascade = cv2.CascadeClassifier("frontal_stop_sign_cascade.xml")
interzis_cascade = cv2.CascadeClassifier("interzis.xml")
angle = 8


def triangles(conturImg):
    im2, contours, hierarchy = cv2.findContours(
        conturImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.04 * cv2.arcLength(cnt, True), True)

        if len(approx) == 3 and cv2.contourArea(approx) > 700:
            # print "triangle"
            point0 = approx[0][0]
            point1 = approx[1][0]
            point2 = approx[2][0]
            center_x = (point0[0]+point1[0]+point2[0])/3
            center_y = (point0[1]+point1[1]+point2[1])/3

            if thresh[center_y, center_x] != 0:
                break
            if ((np.fabs(point0[1] - point1[1]) < angle and point2[1] < point0[1])
                    or (np.fabs(point2[1] - point1[1]) < angle and point0[1] < point2[1])
                    or (np.fabs(point2[1] - point0[1]) < angle and point1[1] < point0[1])):
                print "forward"
            elif (np.fabs(point0[0] - point1[0]) < angle):
                if (point2[1] > point0[1]):
                    print "stanga"
                else:
                    print "dreapta"

            elif (np.fabs(point2[0] - point1[0]) < angle):
                if (point0[1] > point2[1]):
                    print "stanga"
                else:
                    print "dreapta"

            elif (np.fabs(point2[0] - point0[0]) < angle):
                if (point1[1] > point2[1]):
                    print "stanga"
                else:
                    print "dreapta"

            #cv2.drawContours(gray, [cnt], 0, 255, -1)


def stopSign(image):
    stopSigns = stopSign_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in stopSigns:
        print "stop"
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

def interzis(image):
    signs = interzis_cascade.detectMultiScale(image, 1.3, 4)
    for (x, y, w, h) in signs:
        print "interzis"
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blured = cv2.blur(gray, (11, 11))
    ret, thresh = cv2.threshold(blured, 100, 255, cv2.THRESH_BINARY)

    conturImg = thresh.copy()
    triangles(conturImg)

    stopSign(gray)
    interzis(gray)
    y_size = gray.shape[0]
    edges = cv2.Canny(gray[ y_size/2:y_size],300,300,apertureSize = 3)
    cv2.imshow('edges', edges)

    lines = cv2.HoughLines(edges, 1, np.pi/180, 80)
    if lines is not None:
        for line in lines:
            for rho, theta in line:
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a*rho
                y0 = b*rho
                x1 = int(x0 + 1000*(-b))
                y1 = int(y0 + 1000*(a))+ y_size/2
                x2 = int(x0 - 1000*(-b))
                y2 = int(y0 - 1000*(a))+ y_size/2
                if (theta > np.pi/180*135) or (theta < np.pi/180*45): 
                    cv2.line(gray, (x1,y1), (x2,y2), 255, 2)
                    
                elif (theta < np.pi/180*95) and (theta > np.pi/180*85):
                    cv2.line(gray, (x1,y1), (x2,y2), 0, 2)

    # Display the resulting frame
    cv2.imshow('gray', gray)
    cv2.imshow('thresh', thresh)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
