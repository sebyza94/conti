from threading import Thread
import cv2
import socket
import numpy as np

class Triangles:

    def start(self, conturImg,s):

        # start the thread to read frames from the video stream
        Thread(target=self.triangles, args=(conturImg,s,)).start()
        return self

    def triangles(self, conturImg,s):
        angle = 11

        im2, contours, hierarchy = cv2.findContours(
            conturImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            approx = cv2.approxPolyDP(
                cnt, 0.04 * cv2.arcLength(cnt, True), True)

            if len(approx) == 3 and cv2.contourArea(approx) > 700:
                # print "triangle"
                point0 = approx[0][0]
                point1 = approx[1][0]
                point2 = approx[2][0]
                center_x = (point0[0]+point1[0]+point2[0])/3
                center_y = (point0[1]+point1[1]+point2[1])/3

                if conturImg[center_y, center_x] != 0:
                    break
                if ((np.fabs(point0[1] - point1[1]) < angle and point2[1] < point0[1])
                        or (np.fabs(point2[1] - point1[1]) < angle and point0[1] < point2[1])
                        or (np.fabs(point2[1] - point0[1]) < angle and point1[1] < point0[1])):
                    s.send('1')
                    print "forward"
                elif (np.fabs(point0[0] - point1[0]) < angle):
                    if (point2[1] > point0[1]):
                        s.send('4')
                        print "stanga"
                    else:
                        print "dreapta"
                        s.send('3')

                elif (np.fabs(point2[0] - point1[0]) < angle):
                    if (point0[1] > point2[1]):
                        s.send('4')
                        print "stanga"
                    else:
                        print "dreapta"
                        s.send("3")

                elif (np.fabs(point2[0] - point0[0]) < angle):
                    if (point1[1] > point2[1]):
                        s.send('4')
                        print "stanga"
                    else:
                        print "dreapta"
                        s.send('3')
        return
                #cv2.drawContours(gray, [cnt], 0, 255, -1)
