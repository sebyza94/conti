import numpy as np
import cv2
import random
from picamera import PiCamera
from picamera.array import PiRGBArray
from triangles import Triangles
from videostream import PiVideoStream
import time
import socket
from threading import Thread


vs = PiVideoStream(resolution=(640, 480), framerate=43).start()
time.sleep(2.0)


stopSign_cascade = cv2.CascadeClassifier("frontal_stop_sign_cascade.xml")
interzis_cascade = cv2.CascadeClassifier("interzis17.xml")

forward = 0
left = 0
right = 0
stop = 0
interzis = 0

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
port = 8888
s.connect((host, port))


def triangles(conturImg):
    global left, right, forward
    angle = 10
    detected = 0
    im2, contours, hierarchy = cv2.findContours(
        conturImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.04 * cv2.arcLength(cnt, True), True)

        if len(approx) == 3 and cv2.contourArea(approx) > 170:
            point0 = approx[0][0]
            point1 = approx[1][0]
            point2 = approx[2][0]
            center_x = (point0[0]+point1[0]+point2[0])/3
            center_y = (point0[1]+point1[1]+point2[1])/3

            if conturImg[center_y, center_x] !=0:
                break
            if ((np.fabs(point0[1] - point1[1]) < angle and point2[1] < point0[1])
                    or (np.fabs(point2[1] - point1[1]) < angle and point0[1] < point2[1])
                    or (np.fabs(point2[1] - point0[1]) < angle and point1[1] < point0[1])):
                forward += 1
                # s.send('1')
                detected = 1
                print "forward"

            if (np.fabs(point0[0] - point1[0]) < angle):
                if (point2[1] > point0[1]):
                    left += 1
                    detected = 1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"

            elif (np.fabs(point2[0] - point1[0]) < angle):
                if (point0[1] > point2[1]):
                    left += 1
                    detected = 1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"

            elif (np.fabs(point2[0] - point0[0]) < angle):
                if (point1[1] > point2[1]):
                    left += 1
                    detected =1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"
    return detected


def stopSign(image):
    global stop
    stopSigns = stopSign_cascade.detectMultiScale(image, 1.3, 5)
    for (x, y, w, h) in stopSigns:
        stop += 1
        print "stop"
        return 1
    return 0

def noentry(image):
    global interzis
    interzisSign = interzis_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in interzisSign:
        interzis += 1
        print "interzis"
        break

try:

    while(True):

            # Capture frame-by-frame
        image = vs.read()
        #gray = image[0:image.shape[0]*4/5,image.shape[1]/5:image.shape[1]*4/5]

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blured = cv2.blur(gray, (11, 11))
        ret, thresh = cv2.threshold(blured, 90, 255, cv2.THRESH_BINARY)

        rez = triangles(thresh)
        if 0==rez:
            if 0 == stopSign(gray):
                noentry(gray)
        if left > 1:
            s.send('3')
            time.sleep(1.6)
            left = 0
            right = 0
            stop = 0
            interzis = 0
        elif right > 1:
            s.send('4')
            time.sleep(1.6)
            right = 0
            left = 0
            interzis = 0
        elif forward > 2:
            s.send('1')
            forward = 0
            left = 0
            right = 0 
            interzis = 0
            time.sleep(1.0)
        elif stop > 1:
            s.send('5')
            interzis = 0
            forward = 0
        elif interzis > 6:
            s.send('6')
            interzis = 0
            left = 0
            right = 0
        else:
            s.send('8')  # forward
#            Thread(target = playSound, args=("./music/FORWARD.mp3",)).start()
            time.sleep(0.4)
except KeyboardInterrupt:
    pass
except Exception as inst:
    print(type(inst))    # the exception instance
    print(inst.args)     # arguments stored in .args
    print(inst)    

vs.stop()
