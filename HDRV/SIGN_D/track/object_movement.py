# import the necessary packages
import socket
from collections import deque
import numpy as np
import argparse
import imutils
import cv2
import time
from picamera import PiCamera
from picamera.array import PiRGBArray
from videostream import PiVideoStream

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
port = 8888
s.connect((host, port))


# define the lower and upper boundaries of the "green"
# ball in the HSV color space
# yellowLower = (29, 86, 6)
# yellowUpper = (64, 255, 255)

yellowLower = (10, 112, 80)
yellowUpper = (30, 255, 255)

# initialize the list of tracked points, the frame counter,
# and the coordinate deltas
numbers =14
pts = deque(maxlen=numbers)
counter = 0
(dX, dY) = (0, 0)
direction = ""

vs = PiVideoStream(resolution=(640,480),framerate=10).start()
time.sleep(2.0)

# if a video path was not supplied, grab the reference
# to the webcam    

# keep looping
while True:
    # grab the current frame
    frame = vs.read()

    # if we are viewing a video and we did not grab a frame,
    # then we have reached the end of the video

    # blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # construct a mask for the color "green", then perform
    # a series of dilations and erosions to remove any small
    # blobs left in the mask
    mask = cv2.inRange(hsv, yellowLower, yellowUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # find contours in the mask and initialize the current
    # (x, y) center of the ball
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        # only proceed if the radius meets a minimum size
        if radius > 10:
            # draw the circle and centroid on the frame,
            # then update the list of tracked points
            cv2.circle(frame, (int(x), int(y)), int(radius),
                       (0, 255, 255), 2)
            cv2.circle(frame, center, 5, (0, 0, 255), -1)
            pts.appendleft(center)

            if radius < 60:
                print("forward:" + str(radius))
                s.send('1')
            elif radius > 150:
                print("back" + str(radius))
                s.send('2')

    # loop over the set of tracked points
    for i in np.arange(1, len(pts)):
        # if either of the tracked points are None, ignore
        # them
        if pts[i - 1] is None or pts[i] is None:
            continue

        # check to see if enough points have been accumulated in
        # the buffer
        if counter >= 10:
            if pts[-10] is not None and i == 1:
                # compute the difference between the x and y
                # coordinates and re-initialize the direction
                # text variables
                dX = pts[-10][0] - pts[i][0]
                dY = pts[-10][1] - pts[i][1]
                (dirX, dirY) = ("", "")

                # ensure there is significant movement in the
                # x-direction
                if np.abs(dX) > 20:
                    if np.sign(dX) == 1:
                        dirX = "East"
                        s.send("3")
                    else:
                        dirX = "West"
                        s.send("4")

                # ensure there is significant movement in the
                # y-direction
                if np.abs(dY) > 20:
                    if np.sign(dY) == 1:
                        dirY = "North"
                        s.send("1")
                    else:
                        dirY = "South"

                # handle when both directions are non-empty
                if dirX != "" and dirY != "":
                    direction = "{}-{}".format(dirY, dirX)

                # otherwise, only one direction is non-empty
                else:
                    direction = dirX if dirX != "" else dirY

        # otherwise, compute the thickness of the line and
        # draw the connecting lines
        thickness = int(np.sqrt(numbers / float(i + 1)) * 2.5)
        cv2.line(frame, pts[i - 1], pts[i], (0, 0, 255), thickness)

    # show the movement deltas and the direction of movement on
    # the frame
    cv2.putText(frame, direction, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (0, 0, 255), 3)
    cv2.putText(frame, "dx: {}, dy: {}".format(dX, dY),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                0.35, (0, 0, 255), 1)

    # show the frame to our screen and increment the frame counter
    #cv2.imshow("Frame", frame)
    counter += 1

# cleanup the camera and close any open windows camera.release()
vs.stop()
#cv2.destroyAllWindows()
