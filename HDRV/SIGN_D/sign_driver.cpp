#include "opencv2/opencv.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdint.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "../../global.h"
using namespace std;
using namespace cv;

CvCapture* cap;
CascadeClassifier stopSign_cascade;
IplImage* img;
CvMemStorage *storage = cvCreateMemStorage(0);
CvSeq* contours;  //hold the pointer to a contour in the memory block
CvSeq* result;
IplImage* imgGrayScale;
uint8_t sign;

int SIGN_init(void) {
    cap = cvCaptureFromCAM(CV_CAP_ANY);
    if (!cap) {
        printf("Could not initialize capturing...");
        return -1;
    }

    stopSign_cascade.load( "frontal_stop_sign_cascade.xml" );

   // cvNamedWindow("Detections");
}

IplImage* filtering(IplImage* img) {
    IplImage* filtered = cvCreateImage(cvGetSize(img), 8, 1);
    //converting the original image into grayscale
    cvCvtColor(img, filtered, CV_BGR2GRAY);
    //apply a gaussian filter to reduce noise
    cvSmooth(filtered, filtered, CV_GAUSSIAN, 11, 3);
    //create binary image to future match of shapes
    cvThreshold(filtered, filtered, 100, 255, CV_THRESH_BINARY);
    return filtered;
}

void cleanUp() {
    cvDestroyAllWindows();
    cvReleaseMemStorage(&storage);
    cvReleaseImage(&img);
    cvReleaseImage(&imgGrayScale);
    cvReleaseCapture(&cap);
}
/*
uint8_t signHandler() {
    
    uint8_t * outputBuffer = COM_encodeBuffer((uint8_t)sign, (uint16_t)4095);
    BTD_SETOUTPUTBUFFER(outputBuffer);
    
    return 1;
}

void create_signEvent() {
    Event *signEvent = (Event*)malloc( sizeof(Event));
    signEvent->type    = EVENT_SIGN_;
    signEvent->state   = EVENT_NOT_HANDLED;
    signEvent->handler = signHandler;
    _postEvent(signEvent);
}*/

void stopSignDetection(IplImage* img) {

    std::vector<Rect> stop_signs;
    stopSign_cascade.detectMultiScale( cv::cvarrToMat(img), stop_signs, 2, 1, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30) );

    for ( int i = 0; i < stop_signs.size(); i++ )
    {
        printf("STOP!!!\n");
        Point center( stop_signs[i].x + stop_signs[i].width * 0.5, stop_signs[i].y + stop_signs[i].height * 0.5 );
        cv::ellipse( cv::cvarrToMat(img), center, Size( stop_signs[i].width * 0.5, stop_signs[i].height * 0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
    }
}

void detectTriangles() {
    cvFindContours(imgGrayScale, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
    //iterating through each contour
    while (contours)
    {
        //obtain a sequence of points of contour, pointed by the variable 'contour'
        result = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, cvContourPerimeter(contours) * 0.02, 0);
        sign =0;
        //if there are 3  vertices  in the contour(It should be a triangle)
        if (fabs(cvContourArea(result, CV_WHOLE_SEQ)) > 800 )
            if (result->total == 3 )
            {
                //iterating through each point
                CvPoint *pt[3];
                for (int i = 0; i < 3; i++) {
                    pt[i] = (CvPoint*)cvGetSeqElem(result, i);
                }

                if (    (fabs(pt[0]->y - pt[1]->y) < 8 && pt[2]->y < pt[0]->y) ||
                        (fabs(pt[2]->y - pt[1]->y) < 8 && pt[0]->y < pt[2]->y) ||
                        (fabs(pt[2]->y - pt[0]->y) < 8 && pt[1]->y < pt[0]->y)
                   )
                    printf("Forward\n"); //sign =FORWARD;}

                else if (fabs(pt[0]->x - pt[1]->x) < 5)
                    if (pt[2]->y > pt[0]->y)
                        printf("stanga\n");// sign = MAKELEFT;}
                    else printf("dreapta\n");// sign = MAKERIGHT;}

                else if (fabs(pt[2]->x - pt[1]->x) < 5)
                    if (pt[0]->y > pt[2]->y)
                        printf("stanga\n"); //sign = MAKELEFT;}
                    else printf("dreapta\n");// sign = MAKERIGHT;}

                else if (fabs(pt[2]->x - pt[0]->x) < 5)
                    if (pt[1]->y > pt[2]->y)
                        printf("stanga\n");// sign = MAKELEFT;}
                    else printf("dreapta\n");// sign = MAKERIGHT;}

                //drawing lines around the triangle
                cvLine(img, *pt[0], *pt[1], cvScalar(255, 0, 0), 4);
                cvLine(img, *pt[1], *pt[2], cvScalar(255, 0, 0), 4);
                cvLine(img, *pt[2], *pt[0], cvScalar(255, 0, 0), 4);
            }
        //obtain the next contour
        contours = contours->h_next;
        //if(sign !=0) create_signEvent();
	
    }
}

int main() {

    SIGN_init();
    for (;;) {
        img = cvQueryFrame(cap);

        stopSignDetection(img);

        imgGrayScale = filtering(img);
        detectTriangles();

        //show the image in which identified shapes are marked
        cvShowImage("Detections", img);

        if (cvWaitKey(30) >= 0 )
            break; //wait for a key press
    }
    cleanUp();
    return 0;
}
