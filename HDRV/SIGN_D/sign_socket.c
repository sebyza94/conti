#include <stdio.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdint.h>
#include "../../global.h"
#include <sys/socket.h>
#include<string.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>

uint8_t sign;

uint8_t signHandler() {
    return 1;
}

void* SIGN_run(void *arg) {
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
    char client_message[10];

    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8888 );

    //Bind
    if ( bind(socket_desc, (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("bind failed. Error");
        return (void *)1;
    }
    puts("bind done");

    //Listen
    listen(socket_desc , 3);
    while (1) {
        //Accept and incoming connection
        puts("Waiting for incoming connections...");
        c = sizeof(struct sockaddr_in);

        //accept connection from an incoming client
        client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
        if (client_sock < 0)
        {
            perror("accept failed");
            return (void *)1;
        }
        puts("Connection accepted");

        //Receive a message from client
        while ( (read_size = recv(client_sock , client_message , 10 , 0)) > 0 )
        {
            //Send the message back to client
            printf("%s\n", client_message);
            sign = client_message[0] - '0';
            switch (sign) {
            case FORWARD: MD_ForwardWalls(45, 4); break;
            case BACKWARD: MD_BACKWARD(43, 5); break;
            case MAKELEFT: MD_SIGN_Left(47);   break;
            case MAKERIGHT: MD_SIGN_Right(47); break;

            case STOP: MD_CAR_BREAK(30); break;
	    case 6:  {MD_ForwardWalls(45,10); MD_interzis();} break; 
	
            case 8: MD_ForwardWalls(40,2);break;
            }

            memset(client_message, 0, strlen(client_message));
        }
        if (read_size == 0)
        {
            puts("Client disconnected");
            fflush(stdout);
        }
        else if (read_size == -1)
        {
            perror("recv failed");
            break;
        }

    }
    return 0;
}
