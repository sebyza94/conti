import numpy as np
import cv2
import random
from picamera import PiCamera
from picamera.array import PiRGBArray
from triangles import Triangles
from videostream import PiVideoStream
import time
import socket

vs = PiVideoStream().start()
time.sleep(2.0)


stopSign_cascade = cv2.CascadeClassifier("frontal_stop_sign_cascade.xml")
interzis_cascade = cv2.CascadeClassifier("interzis.xml")

left = 0
right = 0
forward = 0
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
port = 8888
s.connect((host, port))


def triangles(conturImg):
    global left,right,forward
    angle = 11

    im2, contours, hierarchy = cv2.findContours(
        conturImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.04 * cv2.arcLength(cnt, True), True)

        if len(approx) == 3 and cv2.contourArea(approx) > 700:
            # print "triangle"
            point0 = approx[0][0]
            point1 = approx[1][0]
            point2 = approx[2][0]
            center_x = (point0[0]+point1[0]+point2[0])/3
            center_y = (point0[1]+point1[1]+point2[1])/3

            if conturImg[center_y, center_x] != 0:
                break
            if ((np.fabs(point0[1] - point1[1]) < angle and point2[1] < point0[1])
                    or (np.fabs(point2[1] - point1[1]) < angle and point0[1] < point2[1])
                    or (np.fabs(point2[1] - point0[1]) < angle and point1[1] < point0[1])):
                forward += 1
                s.send('1')
                print "forward"
            elif (np.fabs(point0[0] - point1[0]) < angle):
                if (point2[1] > point0[1]):
                    left += 1
                    #s.send('4')
                    print "stanga"
                else:
                    right += 1
                    print "dreapta"
                    #s.send('3')

            elif (np.fabs(point2[0] - point1[0]) < angle):
                if (point0[1] > point2[1]):
                    left += 1
                    #s.send('4')
                    print "stanga"
                else:
                    right += 1
                    print "dreapta"
                    #s.send("3")

            elif (np.fabs(point2[0] - point0[0]) < angle):
                if (point1[1] > point2[1]):
                    #s.send('4')
                    left += 1
                    print "stanga"
                else:
                    right += 1
                    print "dreapta"
                    #s.send('3')
            #cv2.drawContours(gray, [cnt], 0, 255, -1)


def stopSign(image):
    stopSigns = stopSign_cascade.detectMultiScale(image, 1.3, 5)
    for (x, y, w, h) in stopSigns:
        s.send('5')
        print "stop"
        cv2.rectangle(image, (x, y), (x+w, y+h), (255, 0, 0), 2)
'''
def noEntry(image):
    print "no entry"
    circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1.2, 100)
    if circles is not None:
        print "interzis"
'''
try:

    while(True):

            # Capture frame-by-frame
        image = vs.read() 
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        average1 = 0
        total1 = 1
        average2 = 0
        total2 = 1
        
        y_size =gray.shape[0]
        x_size=gray.shape[1]
        # Our operations on the frame come here
        sign_roi = gray[0:y_size/3.5, x_size/4: x_size*3/4]

        blured = cv2.blur(sign_roi, (11, 11))
        ret, thresh = cv2.threshold(blured, 70, 255, cv2.THRESH_BINARY)


        conturImg = thresh.copy()
        triangles(conturImg)
        #Triangles().start(conturImg,s)
        stopSign(sign_roi)
        #noEntry(gray)

        edges = cv2.Canny(gray[ y_size/2:y_size],300,300,apertureSize = 3)
        #cv2.imshow('edges', edges)


        lines = cv2.HoughLines(edges, 1, np.pi/180, 80)
        if lines is not None:
            for line in lines:
                for rho, theta in line:
                    #a = np.cos(theta)
                    #b = np.sin(theta)
                    #x0 = a*rho
                    #y0 = b*rho
                    #x1 = int(x0 + 1000*(-b))
                    #y1 = int(y0 + 1000*(a))+ y_size/2
                    #x2 = int(x0 - 1000*(-b))
                    #y2 = int(y0 - 1000*(a))+ y_size/2

                    if (theta > np.pi/180*135):
                        #Irint theta* 180/np.pi, "- st-"
                        average1 = average1 + ((theta* 180/np.pi - average1) / total1)
                        #cv2.line(gray, (x1,y1), (x2,y2), 100, 2)
                    if (theta < np.pi/180*45):
                        average2 = average2 + ((theta* 180/np.pi - average2) / total2)
                        #print theta* 180/np.pi, "---dr--"
                        #cv2.line(gray, (x1,y1), (x2,y2), 255, 2)
                    #elif (theta < np.pi/180*95) and (theta > np.pi/180*85):
                     #   orizont = 1      
#        print average1,"--"
#        print average2,"=="
        #if state==3 or state == 4:
        #    s.send(str(state))
            # print "orizont",orizont
        #    state=1
        if left >= 3:
            s.send('4')
            left = 0
            print "STANGA"
        elif right >= 2:
            s.send('3')
            right = 0
            print "DREAPTA"
        elif forward >=3:
            s.send('1')
            forward=0
            print "FATA"
        if average1 == 0 and average2 !=0:
            s.send('6')
        elif average2 == 0 and average1 !=0:
            s.send('7')
        elif average1 ==0 and average2 ==0:
            r= random.random()
            if r < 0.3:
                s.send('6')
            elif r>0.9:
                s.send('7')
        else:
            s.send('8') #forward

        # Display the resulting frame
        #cv2.imshow('gray', gray)
        #cv2.imshow('thresh', thresh)
       # cv2.imshow('conturImg',conturImg)
        #cv2.imshow('edges', edges)
        #rawCapture.truncate(0)
        #if cv2.waitKey(1) & 0xFF == ord('q'):
        #    break
    # When everything done, release the capture
except KeyboardInterrupt:
    pass
cv2.destroyAllWindows()
vs.stop()                                   
