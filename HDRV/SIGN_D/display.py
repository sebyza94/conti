import numpy as np
import cv2
from picamera import PiCamera
from picamera.array import PiRGBArray
from videostream import PiVideoStream
import time
import socket

#(320,240)
vs = PiVideoStream(resolution=(640,480), framerate=43).start()
time.sleep(2.0)


stopSign_cascade = cv2.CascadeClassifier("frontal_stop_sign_cascade.xml")
interzis_cascade = cv2.CascadeClassifier("interzis17.xml")


sign_stop = cv2.imread('./poze/stop.png',cv2.IMREAD_COLOR)
sign_left = cv2.imread('./poze/stanga.png',cv2.IMREAD_COLOR)
sign_right = cv2.imread('./poze/dreapta.png',cv2.IMREAD_COLOR)
sign_forward = cv2.imread('./poze/forward.jpg',cv2.IMREAD_COLOR)
sign_interzis = cv2.imread('./poze/noentry.png',cv2.IMREAD_COLOR)

forward = 0
left = 0
right = 0
stop = 0
interzis = 0

def triangles(conturImg):
    global left, right, forward
    angle = 10
    detected = 0
    im2, contours, hierarchy = cv2.findContours(
        conturImg, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.04 * cv2.arcLength(cnt, True), True)

        if len(approx) == 3 and cv2.contourArea(approx) > 170:
            point0 = approx[0][0]
            point1 = approx[1][0]
            point2 = approx[2][0]
            center_x = (point0[0]+point1[0]+point2[0])/3
            center_y = (point0[1]+point1[1]+point2[1])/3

            if conturImg[center_y, center_x] !=0:
                break
            if ((np.fabs(point0[1] - point1[1]) < angle and point2[1] < point0[1])
                    or (np.fabs(point2[1] - point1[1]) < angle and point0[1] < point2[1])
                    or (np.fabs(point2[1] - point0[1]) < angle and point1[1] < point0[1])):
                forward += 1
                # s.send('1')
                detected = 1
                print "forward"

            if (np.fabs(point0[0] - point1[0]) < angle):
                if (point2[1] > point0[1]):
                    left += 1
                    detected = 1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"

            elif (np.fabs(point2[0] - point1[0]) < angle):
                if (point0[1] > point2[1]):
                    left += 1
                    detected = 1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"

            elif (np.fabs(point2[0] - point0[0]) < angle):
                if (point1[1] > point2[1]):
                    left += 1
                    detected =1
                    print "stanga"
                else:
                    right += 1
                    detected = 1
                    print "dreapta"
    return detected


def stopSign(image):
    global stop
    stopSigns = stopSign_cascade.detectMultiScale(image, 1.3, 5)
    for (x, y, w, h) in stopSigns:
        print "stop"
        stop += 1
        return 1
    return 0

def noEntry(image):
    global interzis
    interzisSign = interzis_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in interzisSign:
        interzis += 1
        print "interzis"
        break

 
cv2.namedWindow('image')#, cv2.WINDOW_NORMAL)

while(True):

    # Capture frame-by-frame
    image = vs.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    blured = cv2.blur(gray, (9, 9))
    ret, thresh = cv2.threshold(blured, 97, 255, cv2.THRESH_BINARY)
    
    #cv2.imshow("thresh", thresh)
#    triangles(thresh)
    if 0 == triangles(thresh):
        if 0 == stopSign(gray):
            noEntry(gray)


    if forward>1:
        cv2.imshow('image',sign_forward)
        forward = 0
        interzis = 0 
    elif left>1:
        cv2.imshow('image',sign_left)
        left = 0
        interzis = 0
    elif right>1:
        cv2.imshow('image',sign_right)
        right = 0
        interzis = 0
    elif stop>1:
        cv2.imshow('image',sign_stop)
        stop = 0
    elif interzis>7:
        cv2.imshow('image',sign_interzis)
        interzis = 0

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
# When everything done, release the capture
cv2.destroyAllWindows()
vs.stop() 
