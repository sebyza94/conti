#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include "pwm_driver.h"
#include <wiringPiI2C.h>
#include <wiringPi.h>
extern int errno;

int fd_pwm;

void PWM_setAllPWM(uint8_t on, uint8_t off) {

	wiringPiI2CWriteReg8(fd_pwm, __ALL_LED_ON_L, on & 0xFF);
	wiringPiI2CWriteReg8(fd_pwm, __ALL_LED_ON_H, on >> 8);
	wiringPiI2CWriteReg8(fd_pwm, __ALL_LED_OFF_L, off & 0xFF);
	wiringPiI2CWriteReg8(fd_pwm, __ALL_LED_OFF_H, off >> 8);
}

void PWM_setFreq(uint8_t freq) {

	int prescale = (int)(25000000.0f / (4096 * freq) - 0.5f);

	int settings = wiringPiI2CReadReg8(fd_pwm, __MODE1) & 0x7F;			// Set restart bit to 0
	int sleep	= settings | 0x10;									// Set sleep bit to 1
	int wake 	= settings & 0xEF;									// Set sleep bit to 0
	int restart = wake | 0x80;
	wiringPiI2CWriteReg8(fd_pwm, __MODE1, sleep);
	wiringPiI2CWriteReg8(fd_pwm, __PRESCALE, prescale);
	wiringPiI2CWriteReg8(fd_pwm, __MODE1, wake);
	delay(5);
	wiringPiI2CWriteReg8(fd_pwm, __MODE1, restart);
}

void PWM_setPWM( uint8_t channel, uint8_t on, uint8_t off) {

	wiringPiI2CWriteReg8(fd_pwm, __LED0_ON_L + 4 * channel, on & 0xFF);
	wiringPiI2CWriteReg8(fd_pwm, __LED0_ON_H + 4 * channel, on >> 8);
	wiringPiI2CWriteReg8(fd_pwm, __LED0_OFF_L + 4 * channel, off & 0xFF);
	wiringPiI2CWriteReg8(fd_pwm, __LED0_OFF_H + 4 * channel, off >> 8);
}

void PWM_init(void) {

	if ((fd_pwm =  wiringPiI2CSetup (PWM_MODULE_ADRESS)) < 0) {
		fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	PWM_setAllPWM(0, 0);
	wiringPiI2CWriteReg8(fd_pwm, __MODE2, __OUTDRV);
	wiringPiI2CWriteReg8(fd_pwm, __MODE1, __ALLCALL);
	delay(5);
	int mode1 = wiringPiI2CReadReg8(fd_pwm, __MODE1);
	mode1 = mode1 & ~(__SLEEP);
	wiringPiI2CWriteReg8(fd_pwm, __MODE1, mode1);
	delay(5);
}

int main(){
	PWM_init();
	while(1){
		PWM_setPWM(0,15,22);
		delay(5);
	}
}
