#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <wiringSerial.h>
#include "../../../global.h"
#include <sys/stat.h>
#include <sys/types.h>

//#define PIN_WAKE_SW   #pinNumber
int fd; // file descriptor
uint8_t file_opened = 0;
uint8_t _inputBuffer[LEN_BUFFER];
uint8_t _outputBuffer[LEN_BUFFER];
uint8_t outputBufferLEN = 0;


uint8_t bt_setOutputBuffer(uint8_t *buffer) {

	memcpy(_outputBuffer,buffer,MAX_LEN_PROTOCOL);
	outputBufferLEN = MAX_LEN_PROTOCOL;
	return 1;
}

unsigned char bt_ReceiveHandler() {
	tProtocol *message = COM_STACK_DEC(_inputBuffer);
	printf("Interf:%d,data0:%u,data1:%u\n", message->interface, message->data[0], message->data[1]);
	
	memset(_inputBuffer, 0, sizeof(_inputBuffer));
	fflush(stdout);
	if (message->interface == FORWARD)
	{
		printf("\ninainte\n"); fflush(stdout);
		MD_FORWARD(message->data[1], message->data[0]);
	}
	else if (message->interface == BACKWARD)
		MD_BACKWARD(message->data[1], message->data[0]);
	else if (message->interface == MAKELEFT)
		MD_MAKELEFT(message->data[1], message->data[0]);
	else if (message->interface == MAKERIGHT)
		MD_MAKERIGHT(message->data[1], message->data[0]);
	else if (message->interface == STOP)
		MD_CAR_BREAK(message->data[0]);

	return 1;
}

int BTD_init()
{
	if ( (fd = serialOpen(INTERFACE, BAUD_RATE)) < 0)
	{
		fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
		return 0;
	}
	return 1;
}


void* BTD_run(void *arg)
{
	printf("Bluetooth running...\n"); fflush(stdout);
	while (1)
	{
		uint8_t index = 0;
		uint8_t byteArrived;
		while (serialDataAvail(fd))
		{
			byteArrived = serialGetchar(fd);
			if (byteArrived != 0)
			{
				_inputBuffer[index++] = byteArrived;
				
			}
			if (index == MAX_LEN_PROTOCOL)
			{
				// Create event and post it
				int i;
				for (i = 0; i < MAX_LEN_PROTOCOL; i++)
					printf("%u", _inputBuffer[i]);
				fflush(stdout);
				Event *btEvent = (Event*)malloc( sizeof(Event));
				btEvent->type    = EVENT_BT_;
				btEvent->state   = EVENT_NOT_HANDLED;
				btEvent->handler = bt_ReceiveHandler;
				_postEvent(btEvent);
				index = 0;
			}
		}
		if (outputBufferLEN == MAX_LEN_PROTOCOL)
		{
			int i;
			char *p = malloc(sizeof(outputBufferLEN) + 1);
			for (i = 0; i < outputBufferLEN; i++){
				*(p+ i) = _outputBuffer[i];
				//SerialPuts(fd, _outputBuffer[i]);
			}
			*(p+i) = '\n';
			serialPuts(fd,p);
			outputBufferLEN = 0;
			serialFlush (fd);
		}
	}
	return NULL;
}
