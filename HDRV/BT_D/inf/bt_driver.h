#ifndef BT_D
#define BT_D

#define LEN_BUFFER 6
#define MAX_LEN_PROTOCOL 5
#define BAUD_RATE 9600 
#define INTERFACE "/dev/ttyAMA0" // "/dev/rfcomm0" 
typedef unsigned char uint8_t;

int BTD_init();
void* BTD_run(void *arg);
uint8_t *bt_getInputBuffer();
uint8_t  bt_setOutputBuffer(uint8_t *buffer);
#endif
