#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <wiringPi.h>
#include "../../../global.h"

#define delay1st 15
#define delay2st 13

#define delay1dr 8
#define delay2dr 6

#define distanceLeft 600
#define distanceRight 550
#define delayToIntersectionLeft  30
#define delayToIntersectionRight 300
#define delaySpin 730
#define addPWM 40

void MD_init(void)
{

	pinMode (PWM_ENABLE, PWM_OUTPUT);
	pinMode (MOTOR_ST_FORWARD, OUTPUT);
	pinMode (MOTOR_ST_BACKWARD, OUTPUT);
	pinMode (MOTOR_DR_FORWARD, OUTPUT);
	pinMode (MOTOR_DR_BACKWARD, OUTPUT);
}


uint8_t MD_ForwardWalls(uint8_t dutycycle, uint8_t duration) {

	if (dutycycle > PWMrange)
		dutycycle = PWMrange;

	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);


	int SEN_left; // =  getSensorData(SEN_LEFT);
	int SEN_right; // =  getSensorData(SEN_RIGHT);

	unsigned int start = millis();

	while (start + 100 * duration > millis()) {
		//digitalWrite (MOTOR_ST_FORWARD, HIGH);
		//digitalWrite (MOTOR_DR_FORWARD, HIGH);
		SEN_left  =  getSensorData(SEN_LEFT);
		SEN_right  =  getSensorData(SEN_RIGHT);


		if (SEN_right >  distanceRight) {
			digitalWrite(MOTOR_ST_FORWARD, LOW);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			if (dutycycle < 45)
				pwmWrite(PWM_ENABLE,pwm +addPWM);
			delay(delay1dr);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			delay(delay2dr);
			pwmWrite(PWM_ENABLE,pwm);

		} else if (SEN_left > distanceLeft) {
			digitalWrite(MOTOR_DR_FORWARD, LOW);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			if(dutycycle < 45)
				pwmWrite(PWM_ENABLE, pwm +addPWM);
			delay(delay1st);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			delay(delay2st);
			pwmWrite(PWM_ENABLE, pwm);

		} else {
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
		}

	}
	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, LOW);
	pwmWrite(PWM_ENABLE, 0);
	return 1;
}

uint8_t MD_SIGN_Right(uint8_t dutycycle) {

	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	int SEN_left; // =  getSensorData(SEN_LEFT);
	int SEN_right =  getSensorData(SEN_RIGHT);

	while (SEN_right > 75) {

		SEN_left  =  getSensorData(SEN_LEFT);
		SEN_right  =  getSensorData(SEN_RIGHT);


		if (SEN_right >  distanceRight) {
			digitalWrite(MOTOR_ST_FORWARD, LOW);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			if (dutycycle < 45)
				pwmWrite(PWM_ENABLE,pwm +addPWM);
			delay(delay1dr);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			delay(delay2dr);
			pwmWrite(PWM_ENABLE, pwm);

		} else if (SEN_left > distanceLeft) {
			digitalWrite(MOTOR_DR_FORWARD, LOW);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			if (dutycycle < 45)
				pwmWrite(PWM_ENABLE,pwm +addPWM);
			delay(delay1st);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			delay(delay2st);
			pwmWrite(PWM_ENABLE, pwm);

		} else {
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
		}

	}
	delay(delayToIntersectionRight);
	MD_Right90();

	return 1;

}

uint8_t MD_SIGN_Left(uint8_t dutycycle) {
	int SEN_right;//  =  getSensorData(SEN_RIGHT);
	int SEN_left = getSensorData(SEN_LEFT);
	
	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	while (SEN_left > 75) {
		SEN_left  =  getSensorData(SEN_LEFT);
		SEN_right  =  getSensorData(SEN_RIGHT);


		if (SEN_right >  distanceRight) {
			digitalWrite(MOTOR_ST_FORWARD, LOW);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			delay(delay1dr);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			delay(delay2dr);

		} else if (SEN_left > distanceLeft) {
			digitalWrite(MOTOR_DR_FORWARD, LOW);
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			delay(delay1st);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
			delay(delay2st);

		} else {
			digitalWrite(MOTOR_ST_FORWARD, HIGH);
			digitalWrite(MOTOR_DR_FORWARD, HIGH);
		}
		//	printf("sen:%d\n", SEN_right);
	}
	delay(delayToIntersectionLeft);
	MD_Left90();
	return 1;
}

uint8_t MD_Right90() {

	printf("Move right 90 \n");
	pwmWrite(PWM_ENABLE, (int)(10.23*47));


	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, HIGH);

	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, LOW);

	int SEN_left; // =  getSensorData(SEN_LEFT);
	int SEN_forward; // =  getSensorData(SEN_RIGHT);

	delay(delaySpin);

	do {
		//digitalWrite (MOTOR_ST_FORWARD, HIGH);
		//digitalWrite (MOTOR_DR_FORWARD, HIGH);
		SEN_left  =  getSensorData(SEN_LEFT);
		SEN_forward  =  getSensorData(SEN_FORWARD);
	} while (SEN_left > 40 && SEN_forward > 40);

	digitalWrite (MOTOR_ST_FORWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_BACKWARD, LOW); // reset the pin to 0
	pwmWrite(PWM_ENABLE, 0);
	delay(400);
	return 1;
}


uint8_t MD_Left90() {

	printf("Move left 90 \n");
	pwmWrite(PWM_ENABLE, (int) 10.23*47);


	digitalWrite (MOTOR_ST_BACKWARD, HIGH);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);

	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	int SEN_right; // =  getSensorData(SEN_LEFT);
	int SEN_forward; // =  getSensorData(SEN_RIGHT);

	delay(delaySpin);

	do {
		//digitalWrite (MOTOR_ST_FORWARD, HIGH);
		//digitalWrite (MOTOR_DR_FORWARD, HIGH);
		SEN_right  =  getSensorData(SEN_RIGHT);
		SEN_forward  =  getSensorData(SEN_FORWARD);
	} while (SEN_right > 40 && SEN_forward > 40);

	digitalWrite (MOTOR_ST_BACKWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_FORWARD, LOW); // reset the pin to 0
	pwmWrite(PWM_ENABLE, 0);
	delay(400);
	return 1;
}


uint8_t MD_interzis(){

	pwmWrite(PWM_ENABLE, (int) 10.23* 45);
	digitalWrite (MOTOR_ST_BACKWARD, HIGH);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);

	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	int SEN_forward;

	delay(900);
	do {
		SEN_forward  =  getSensorData(SEN_FORWARD);
	} while ( SEN_forward > 20);// || SEN_right > 360 || SEN_left > 360));

	
	digitalWrite (MOTOR_ST_BACKWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_FORWARD, LOW); // reset the pin to 0
	pwmWrite(PWM_ENABLE, 0);

	return 1;
}



uint8_t MD_carBreak(uint8_t duration) {
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, LOW);

	pwmWrite(PWM_ENABLE, 0);
	delay(100 * duration);
	return 1;
}

uint8_t MD_Forward(uint8_t dutycycle, uint8_t duration) {
	if (dutycycle > PWMrange)
		dutycycle = PWMrange;
	printf("Move forward dutycycle:%u, duration:%u\n", dutycycle, duration);
	fflush(stdout);
	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	delay(100 * duration);
	digitalWrite (MOTOR_ST_FORWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_FORWARD, LOW); // reset the pin to 0

	pwmWrite(PWM_ENABLE, 0);
	return 1;
}

uint8_t MD_Backward(uint8_t dutycycle, uint8_t duration) {
	if (dutycycle > PWMrange)
		dutycycle = PWMrange;
	printf("Move backard %u, %u\n", dutycycle, duration);
	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);
	digitalWrite (MOTOR_ST_BACKWARD, HIGH);
	digitalWrite (MOTOR_DR_BACKWARD, HIGH);
	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, LOW);

	delay(100 * duration);
	digitalWrite (MOTOR_ST_BACKWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_BACKWARD, LOW); // reset the pin to 0

	pwmWrite(PWM_ENABLE, 0);


	return 1;
}

uint8_t MD_MakeLeft(uint8_t dutycycle, uint8_t duration) {
	if (dutycycle > PWMrange)
		dutycycle = PWMrange;
	printf("Move right %u, %u", dutycycle, duration);
	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	digitalWrite (MOTOR_ST_BACKWARD, HIGH);
	digitalWrite (MOTOR_DR_BACKWARD, LOW);
	digitalWrite (MOTOR_ST_FORWARD, LOW);
	digitalWrite (MOTOR_DR_FORWARD, HIGH);

	delay(100 * duration);
	digitalWrite (MOTOR_ST_BACKWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_FORWARD, LOW); // reset the pin to 0

	pwmWrite(PWM_ENABLE, 0);


	return 1;
}

uint8_t MD_MakeRight(uint8_t dutycycle, uint8_t duration) {
	if (dutycycle > PWMrange)
		dutycycle = PWMrange;
	printf("Move left %u, %u", dutycycle, duration);
	double pwm = 10.23 * dutycycle;
	pwmWrite(PWM_ENABLE, (int) pwm);//dutycycle);
	digitalWrite (MOTOR_ST_BACKWARD, LOW);
	digitalWrite (MOTOR_DR_BACKWARD, HIGH);
	digitalWrite (MOTOR_ST_FORWARD, HIGH);
	digitalWrite (MOTOR_DR_FORWARD, LOW);

	delay(100 * duration);
	digitalWrite (MOTOR_ST_FORWARD, LOW); // reset the pin to 0
	digitalWrite (MOTOR_DR_BACKWARD, LOW); // reset the pin to 0

	pwmWrite(PWM_ENABLE, 0);


	return 1;
}

uint8_t MD_8(){

	MD_Forward(45,10);
	MD_MakeLeft(45,2);
	double pwm = 10.23 * 45; 
	pwmWrite(PWM_ENABLE, (int) pwm);

	unsigned int start = millis();
	
	while (start + 3000 > millis()) {  	
		digitalWrite(MOTOR_DR_FORWARD, LOW);
		digitalWrite(MOTOR_ST_FORWARD, HIGH);

		digitalWrite(MOTOR_DR_BACKWARD, HIGH);
		pwmWrite(PWM_ENABLE, pwm + 36);
		delay(18);
		digitalWrite(MOTOR_DR_BACKWARD, LOW); 
		digitalWrite(MOTOR_DR_FORWARD, HIGH);
		
		delay(10);
		pwmWrite(PWM_ENABLE, pwm);

	}
	MD_Forward(45,10);
	
	start = millis();
	while (start + 4000 > millis()) {  	
		digitalWrite(MOTOR_ST_FORWARD, LOW);
		digitalWrite(MOTOR_DR_FORWARD, HIGH);

		digitalWrite(MOTOR_ST_BACKWARD, HIGH); 

		pwmWrite(PWM_ENABLE, pwm + 36);
		delay(18);
		digitalWrite(MOTOR_ST_FORWARD, HIGH);
		digitalWrite(MOTOR_ST_BACKWARD, LOW); 
		delay(10);
		pwmWrite(PWM_ENABLE, pwm);

	}
	MD_Forward(45,10);
	/*
	while (start + 4000 > millis()) {  	
		digitalWrite(MOTOR_DR_FORWARD, LOW);
		digitalWrite(MOTOR_ST_FORWARD, HIGH);

		digitalWrite(MOTOR_DR_BACKWARD, HIGH);
		pwmWrite(PWM_ENABLE, pwm + 36);
		delay(18);
		digitalWrite(MOTOR_DR_BACKWARD, LOW); 
		digitalWrite(MOTOR_DR_FORWARD, HIGH);
		
		delay(10);
		pwmWrite(PWM_ENABLE, pwm);

	}*/
	MD_carBreak(1);
	return 1;
}
