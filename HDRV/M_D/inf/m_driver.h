#ifndef M_D
#define M_D

#define PWMrange 100
#define PWM_ENABLE 1
#define MOTOR_ST_FORWARD 9
#define MOTOR_ST_BACKWARD 8

#define MOTOR_DR_FORWARD 0
#define MOTOR_DR_BACKWARD 7

typedef unsigned char uint8_t;

void MD_init(void);
uint8_t MD_move(uint8_t dutycycle, uint8_t duration,
                uint8_t motor_forward, uint8_t motor_backward);

uint8_t MD_ForwardWalls(uint8_t dutycycle, uint8_t duration);
uint8_t MD_ForwardEncoders(uint8_t dutycycle, uint8_t duration);

uint8_t MD_Left90();
uint8_t MD_Right90();

uint8_t MD_SIGN_Left(uint8_t dutycycle);
uint8_t MD_SIGN_Right(uint8_t dutycycle);

uint8_t MD_interzis();

uint8_t MD_8();

uint8_t MD_carBreak(uint8_t time);
uint8_t MD_Forward(uint8_t dutycycle, uint8_t duration);
uint8_t MD_Backward(uint8_t dutycycle, uint8_t duration);
uint8_t MD_MakeLeft(uint8_t dutycycle, uint8_t duration);
uint8_t MD_MakeRight(uint8_t dutycycle, uint8_t duration);

#endif
