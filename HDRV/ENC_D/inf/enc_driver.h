#ifndef ENC_D
#define ENC_D

#define LEFT 24    // Pin 0 WiringPi = pin 17  BCM GPIO
#define RIGHT 28    // Pin 1 WiringPi = pin 18  BCM GPIO
#define RESOLUTION 12 // 10 holes

void ENCD_init(void);
void* ENCD_run(void *arg);

uint8_t ENC_getLeft();
uint8_t ENC_getRight();
void ENC_reset();
#endif
