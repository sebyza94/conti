#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <wiringPi.h>
#include "../../../global.h"



static volatile uint16_t globalCounter[2];
uint16_t leftCounter;
uint16_t rightCounter;


void myInterruptLeft  (void) {++globalCounter[0];}// printf("stanga ");fflush(stdout);}
void myInterruptRight (void) {++globalCounter[1];}// printf("dr ");fflush(stdout);}

uint8_t ENC_getLeft() {
	return leftCounter;
}

uint8_t ENC_getRight() {
	return rightCounter;
}

void ENC_reset() {
	globalCounter[0] = 0;
	globalCounter[1] = 0;
}




uint8_t enc_Handler() {
	int deltaCounted = leftCounter - rightCounter;
	printf("Delta: %u\n", deltaCounted);
	if (deltaCounted > 0)
		MD_MAKELEFT(100, 1);
	else if (deltaCounted < 0)
		MD_MAKERIGHT(100, 1);
	globalCounter[0] = 0;
	globalCounter[1] = 0;
	return 1;
}

void* ENCD_run(void *arg) {
	printf("Encoders running...\n"); fflush(stdout);
	while (1) {
		leftCounter  = globalCounter[0];
		rightCounter = globalCounter[1];

	}
	return NULL;
}
void ENCD_init() {
	wiringPiISR(LEFT, INT_EDGE_RISING, &myInterruptLeft);
	wiringPiISR(RIGHT, INT_EDGE_RISING, &myInterruptRight);
}

