#ifndef SEN_D
#define SEN_D

#define BASE 200
#define SPI_CHAN 0

#define SEN_FORWARD 3
#define SEN_BACKWARD 1
#define SEN_LEFT 2
#define SEN_RIGHT 0

void SEND_init(void);
void* SEND_run(void* arg);

int getSensorData(uint8_t channel);
double get_IR (uint16_t value);

#endif
