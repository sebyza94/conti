#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

#include <wiringPi.h>
#include <mcp3004.h>

#include "../../../global.h"

int senzori[4];

void SEND_init(void) {
	mcp3004Setup (BASE, SPI_CHAN);
}

void* SEND_run(void* arg) {
	while (1) {
		senzori[SEN_FORWARD] = analogRead (BASE + SEN_FORWARD);
		senzori[SEN_BACKWARD] = analogRead (BASE + SEN_BACKWARD);
		senzori[SEN_LEFT] = analogRead (BASE + SEN_LEFT);
		senzori[SEN_RIGHT] = analogRead (BASE + SEN_RIGHT);
	//		int i;
	//		delay(100);
	//		for(i=0; i<4; i++)
	//			printf("A[%d]=%d ", i, senzori[i]);
	//		printf("\n");
	}

}

int getSensorData(uint8_t channel) {
	return  senzori[channel];
}

//return distance (cm)
double get_IR (uint16_t value) {
	if (value < 16)  value = 16;
	return (int) (2076.0 / (value - 11.0));
}
