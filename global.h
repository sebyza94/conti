#ifndef GLOBAL
#define GLOBAL

#include "COMM/communicationProtocol.h"
#include "COMM/crc.h"

#include "MAN/EVEXEC/inf/event.h"

#include "HDRV/BT_D/inf/bt_driver.h"
#include "HDRV/M_D/inf/m_driver.h"
#include "HDRV/ENC_D/inf/enc_driver.h"
#include "HDRV/SEN_D/inf/sen_driver.h"
#include "HDRV/SIGN_D/sign_driver.h"

#define BTD_GETINPUTBUFFER (bt_getInputBuffer)
#define BTD_SETOUTPUTBUFFER (bt_setOutputBuffer)
#define BTD_INIT (BTD_init)
#define BTD_RUN (BTD_run)

#define COM_STACK_CRC (crc8)
#define COM_STACK_ENC (COM_encodeBuffer)
#define COM_STACK_DEC(x) (COM_decodeBuffer(x))

#define MD_MOVE (MD_move)
#define MD_FORWARD (MD_Forward)
#define MD_BACKWARD (MD_Backward)
#define MD_MAKELEFT (MD_MakeLeft)
#define MD_MAKERIGHT (MD_MakeRight)
#define MD_CAR_BREAK (MD_carBreak)


#endif
